/* exported AudioObject */
/* global URL, Blob, ArrayBuffer, DataView */

/**
 * @author Martin Mende <martin.mende@aristech.de>
 * @copyright Aristech GmbH 2015
 *
 * @license http://creativecommons.org/licenses/by/4.0/legalcode CC Attribution 4.0 International (CC-BY-4.0)
 */

/**
 * An Audio Object
 */
var AudioObject = function() {

	'use strict';

	/** @type {Object} These are the default properties **/
	var _properties = {
		type:			'audio/wav',
		sampleRate:		44100,
		bitsPerSample:	16,
		channels:		2
	};

	/** @type {Blob} Holds the actual data blob **/
	var _data;
	/** @type {String} Holds the url */
	var _url;

	/**
	 * Initialices AudioObject with data
	 *
	 * @param  {Array} data        The data
	 * @param  {Object} properties The properties
	 *
	 * @return {AudioObject} The AudioObject
	 */
	this.createWithData = function(data, properties)
	{
		// Assure data is an Array
		data = Array.isArray(data) ? data : [data];
		// Assure properties is an Object
		properties = properties || {};
		// Init...
		initWithDataArray(data, properties);
		// Return the AudioObject for chaining
		return this;
	};

	/**
	 * Creates an audio by checking the header
	 *
	 * @param  {Array}   data     The data
	 * @param  {Function} callback The callback
	 */
	this.create = function(data, callback)
	{
		// Assure data is an Array
		data = Array.isArray(data) ? data : [data];

		// Extract properties from header
		getWaveHeaderInfo(data, function(properties){
			// Init...
			initWithDataArray(data, properties);
			// Callback...
			callback(this);
		});
	};

	/**
	 * Initialices AudioObject with a url
	 *
	 * @param  {String}   url        The url
	 * @param  {Object}   properties The properties
	 * @param  {Function} callback   A callback function
	 */
	this.createFromURL = function(url, properties, callback)
	{
		// Assure callback exists
		callback = callback || function(){};

		// Check if properties need to be fetched from the header
		var propertiesProvided = Object.keys(properties).length > 0;
		// Join with the default properties
		joinProperties(properties);
		
		// Save own instance
		var instance = this;

		// Create a xhr request
		var xhr = new XMLHttpRequest();
		xhr.open('GET', url, true);
		xhr.responseType = 'arraybuffer';
		xhr.onload = function(e) {
			if (this.status === 200)
			{
				// Create the data Blob
				var audioBlob = new Blob([this.response], {type: 'audio/wav'});

				// Check if the properties needs to be fetched from the header
				if(propertiesProvided)
				{
					// Init with the data
					instance.createWithData(audioBlob, _properties);

					callback(true, instance);
				}
				else
				{	
					// Extract properties from header
					getWaveHeaderInfo(audioBlob, function(properties){
						// Add the aquired information to the properties
						joinProperties(properties);
						// Init with data
						instance.createWithData(audioBlob, _properties);
						// Callback...
						callback(true, instance);
					});
				}
			}
			else
			{
				callback(false);
				throw 'Audio URL not found: ' + e.responseText;
			}
		};
		xhr.send();
	};

	/** Constructors */
	/** Check what kind of constructor to use **/
	if (arguments.length > 0)
	{
		if(arguments[0] instanceof Blob || Array.isArray(arguments[0]) )
		{
			this.createWithData(arguments[0], arguments[1]);
		}
		else if (typeof arguments[0] === 'string')
		{
			if (typeof arguments[1] === 'function')
			{
				// Call without properties
				this.createFromURL(arguments[0], {}, arguments[1]);
			}
			else
			{
				// Call with properties
				this.createFromURL(arguments[0], arguments[1], arguments[2]);
			}
		}
	}
	/** -Constructors End */

	/**
	 * Joins the passed properties with the default ones
	 *
	 * @param  {Object} properties The properties
	 */
	function joinProperties(properties)
	{
		// Merge the passed properties with the default ones
		for (var key in properties)
		{
			_properties[key] = properties[key];
		}
	}

	/**
	 * Initiates the AudioObject with audio data
	 *
	 * @param  {Array}  data       The audio data
	 * @param  {Object} properties The audio properties
	 */
	function initWithDataArray(data, properties)
	{
		// Merge the passed properties with the default ones
		joinProperties(properties);

		/** Construct the Audio depending on the MimeType **/
		switch (_properties.type)
		{
			case 'audio/wav':
				/** Construct a Wave **/
				// Sum up the dataSize from each data value
				// and create an array with the raw data without headers
				var dataSize = 0;
				var rawData = [];
				for (var i = 1; i <= data.length; i++) {// The first value is reserved for the header
					dataSize += data[i-1].size - 44;
					rawData[i] = data[i-1].slice(44);
				}
				// Create the blob with combined header and the raw data array
				var header = constructWaveHeader(dataSize);
				rawData[0] = header;
				_data = new Blob(rawData, {type: _properties.type});
				break;
				/** -Wave End */
			default:
				throw 'Could not create audio with mimeType ' + properties.type;
		}
	}

	/**
	 * Writes a String to a DataView
	 *
	 * @param  {String}   s  The String
	 * @param  {DataView} dv The DataView
	 * @param  {Number} p The offset inside the DataView
	 *
	 * @return {Number}   The new offset
	 */
	function writeString(s, dv, p) {
		for (var i = 0; i < s.length; i++) {
			dv.setUint8(p + i, s.charCodeAt(i));
		}
		p += s.length;
		return p;
	}
 	/**
 	 * Writes an unsigned 32-bit Number to a DataView
 	 *
 	 * @param  {Number} d    The Number
 	 * @param  {DataView} dv The DataView
 	 * @param  {Number} p The offset inside the DataView
 	 *
 	 * @return {Number}   The new offset
 	 */
	function writeUint32(d, dv, p) {
		dv.setUint32(p, d, true);
		return p + 4;
	}
 	/**
 	 * Writes an unsigned 16-bit Number to a DataView
 	 *
 	 * @param  {Number} d    The Number
 	 * @param  {DataView} dv The DataView
 	 * @param  {Number} p The offset inside the DataView
 	 *
 	 * @return {Number}   The new offset
 	 */
	function writeUint16(d, dv, p) {
		dv.setUint16(p, d, true);
		return p + 2;
	}

	/**
	 * Constructs a wave header
	 *
	 * @param  {Object} properties      The properties {frames, (channels), (sampleRate), (bitRate)}
	 *
	 * @return {ArrayBuffer}            The header
	 */
	function constructWaveHeader(dataSize)
	{
		/** @type {Number} The blockAlign **/
		var blockAlign = _properties.channels * (_properties.bitsPerSample / 8);
		/** @type {Number} The byteRate **/
		var byteRate = _properties.sampleRate * blockAlign;
		/** @type {Number} The chunkSize **/
		var chunkSize = (dataSize / 8) - 8;
	 
		var buffer = new ArrayBuffer(44);
		var dv = new DataView(buffer);
	 
		var p = 0;
	 
		p = writeString('RIFF', dv, p);						// ChunkID
		p = writeUint32(chunkSize, dv, p);					// ChunkSize
		p = writeString('WAVE', dv, p);						// Format
		p = writeString('fmt ', dv, p);						// Subchunk1ID
		p = writeUint32(16, dv, p);							// Subchunk1Size
		p = writeUint16(1, dv, p);							// AudioFormat (PCM=1)
		p = writeUint16(_properties.channels, dv, p);		// Channels
		p = writeUint32(_properties.sampleRate, dv, p);		// SampleRate
		p = writeUint32(byteRate, dv, p);					// ByteRate
		p = writeUint16(blockAlign, dv, p);					// BlockAlign
		p = writeUint16(_properties.bitsPerSample, dv, p);	// BitsPerSample
		p = writeString('data', dv, p);						// Subchunk2ID
		p = writeUint32(dataSize, dv, p);					// Subchunk2Size
	 
		return buffer;
	}

	/**
	 * Extracts the wave header information
	 *
	 * @param {Blob} data The data to observe
	 * @param {Function} callback A callback method recieving the infos
	 */
	function getWaveHeaderInfo(data, callback)
	{
		// Get an ArrayBuffer with the header
		var headerBlob = data.slice(0, 44);

		var buffer;

		var uint8ArrayNew  = null;
		var arrayBufferNew = null;
		var fileReader     = new FileReader();
		fileReader.onload  = function() {

			var dv = new DataView(this.result);

			/*var riff =	String.fromCharCode(dv.getUint8(0)) + 
						String.fromCharCode(dv.getUint8(1)) + 
						String.fromCharCode(dv.getUint8(2)) + 
						String.fromCharCode(dv.getUint8(3));*/
			var infos = {};

			infos.type = 'audio/wav';
			infos.channels = dv.getUint16(22, true);
			infos.sampleRate = dv.getUint32(24, true);
			infos.bitsPerSample = dv.getUint16(34, true);

			//console.log(callback);
			callback(infos);
		};
		fileReader.readAsArrayBuffer(headerBlob);
	}

	/**
	 * Returns the header size
	 *
	 * @return {Number} [description]
	 */
	var headerSize = function()
	{
		return 44;
	};

	/**
	 * Returns the bytes per second
	 *
	 * @return {Number} The bytes per second
	 */
	var bytesPerSecond = function()
	{
		return (_properties.sampleRate * _properties.bitsPerSample * _properties.channels) / 8;
	};

	/**
	 * Returns a subpart of the wave in a given time frame
	 *
	 * @param  {Number} start The time in seconds to start the frame
	 * @param  {Number} end   The time in seconds from the end of the audio
	 *
	 * @return {Blob}   The desired audio part
	 */
	function partOfWave(start, end)
	{
		/** @type {Number} The index where to start the slice **/
		var sliceStart = headerSize() + (start * bytesPerSecond());
		/** @type {Number} The index where to end the slice **/
		var sliceEnd = (end * bytesPerSecond()) * -1;

		// Assure the indices are in a valid range
		sliceStart = sliceStart < _data.size ? sliceStart : headerSize();
		sliceEnd = sliceEnd * -1 < _data.size ? sliceEnd : _data.size;
		sliceEnd = sliceEnd === -0 ? _data.size : sliceEnd;

		/** @type {Number} The dataSize in bits without the header size **/
		var dataSize = ((_data.size - headerSize()) - (sliceStart + headerSize() + (sliceEnd * -1)));
		/** @type {Blob} The actual header */
		var header = new Blob([constructWaveHeader(dataSize)]);
		/** @type {Blob} The data part */
		var data = _data.slice(sliceStart, sliceEnd);

		// Create the new Blob and return it
		return new Blob([header, data], {type: _properties.type});
	}

	/**
	 * Cuts away a specific part with the given time
	 *
	 * @param  {Number} start The time in seconds to start the cut
	 * @param  {Number} end   The time in seconds from the end of the audio
	 */
	function cutPartOfWave(start, end)
	{
		/** @type {Number} The index where to start the slice **/
		var sliceStart = headerSize() + (start * bytesPerSecond());
		/** @type {Number} The index where to end the slice **/
		var sliceEnd = (end * bytesPerSecond()) * -1;

		// Assure the indices are in a valid range
		sliceStart = sliceStart < _data.size ? sliceStart : headerSize();
		sliceEnd = sliceEnd * -1 < _data.size ? sliceEnd : _data.size;
		sliceEnd = sliceEnd === -0 ? _data.size : sliceEnd;

		/** Compute the new _data stuff **/
		// Fetch the raw-data arround the desired cut
		var part1 = _data.slice(headerSize(), sliceStart);
		var part2 = _data.slice(sliceEnd);
		/** @type {Nulber} The dataSize without header **/
		var dataSize = part1.size + part2.size;
		/** @type {Blob} The actual header **/
		var header = new Blob([constructWaveHeader(dataSize)]);

		/** Compute the cut itself **/
		var cut = partOfWave(start, end);

		// Create the new Blob and set it to _data
		_data = new Blob([header, part1, part2], {type: _properties});
		// Return the cut
		return cut;
	}

	/**
	 * Returnes an audio with another AudioObject pasted into this ones
	 *
	 * @param  {AudioObject} audioObject The AudioObject to paste
	 * @param  {Number}      second      Where to paste the audio
	 *
	 * @return {Blob}                    The combined audio
	 */
	function pastePartIntoWave(audioObject, second)
	{		
		/** @type {Number} The index where to split the _data **/
		var splitAt = headerSize() + (second * bytesPerSecond());

		// Fetch the three raw-data parts
		var part1 = _data.slice(headerSize(), splitAt);
		var part2 = audioObject.blob().slice(headerSize());
		var part3 = _data.slice(splitAt);

		/** @type {Number} The new dataSize **/
		var dataSize = part1.size + part2.size + part3.size;
		/** @type {Blob} The actual header */
		var header = new Blob([constructWaveHeader(dataSize)]);

		// Create the resulting Blob
		return new Blob([header, part1, part2, part3], _properties);
	}

	/**
	 * Returns the properties
	 *
	 * @return {Object} The properties
	 */
	this.properties = function()
	{
		return _properties;
	};

	/**
	 * Returns the actual data Blob
	 *
	 * @return {Blob} The data Blob
	 */
	this.blob = function()
	{
		return _data;
	};

	/**
	 * Returns the size of the audio in bits
	 *
	 * @return {Number} The size in bits
	 */
	this.size = function()
	{
		return _data.size;
	};

	/**
	 * Returns the duration in seconds
	 *
	 * @return {Number} The duration
	 */
	this.duration = function()
	{
		var bytesPerSecond = (_properties.sampleRate * _properties.channels * _properties.bitsPerSample) / 8;
		var rawSize = _data.size - 44;
		return rawSize / bytesPerSecond;
	};

	/**
	 * Truncates the audio by the given time
	 *
	 * @param  {Number} start The time in seconds to trim at the start of the audio
	 * @param  {Number} end   The time in seconds to trim at the end of the audio
	 */
	this.truncate = function()
	{
		var start = arguments.length > 0 ? arguments[0] : 0;
		var end = arguments.length > 1 ? arguments[1] : 0;

		// Cut the desired part and set it as new _data
		_data = partOfWave(start, end);

		// Return this for chaining
		return this;
	};

	/**
	 * Returns a subsection of the audio in the given time and removes it from this audio
	 *
	 * @param  {Number} start The second where to start the cut
	 * @param  {Number} end   The second where to end the cut
	 *
	 * @return {AudioObject}  A new AudioObject with the selected part
	 */
	this.cut = function()
	{
		var start = arguments.length > 0 ? arguments[0] : 0;
		var end = arguments.length > 1 ? arguments[1] : 0;
		end = this.duration() - end; // This has to be done because in cutPartOfWave end is seconds from end...

		return new AudioObject(cutPartOfWave(start, end), _properties);
	};

	/**
	 * Returns a subsection of the audio in the given time
	 *
	 * @param  {Number} start The second where to start the cut
	 * @param  {Number} end   The second where to end the cut
	 *
	 * @return {AudioObject}  A new AudioObject with the selected part
	 */
	this.copy = function()
	{
		var start = arguments.length > 0 ? arguments[0] : 0;
		var end = arguments.length > 1 ? arguments[1] : 0;
		end = this.duration() - end; // This has to be done because in partOfWave end is seconds from end...

		return new AudioObject(partOfWave(start, end), _properties);
	};


	/**
	 * Pastes an audioObject at the given second
	 *
	 * @param  {AudioObject} audioObject The AudioObject to paste
	 * @param  {Number}      second    Where to paste the new part
	 */
	this.paste = function(audioObject)
	{
		// If no second is provided or it is outside of the allowed range
		// it will be added to the end
		var cd = this.duration();
		var second = arguments.length <= 1 ? cd : arguments[1] > cd ? cd : arguments[1];

		// Paste the audioObject into this one
		_data = pastePartIntoWave(audioObject, second);

		// Return this for chaining
		return this;
	};


	/**
	 * Returns a url for the audio
	 *
	 * @return {String} The url
	 */
	this.url = function()
	{
		if(_url)
		{
			URL.revokeObjectURL(_url);
		}
		_url = URL.createObjectURL(_data);
		return _url;
	};
};