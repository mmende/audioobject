# README #

A cross-browser implementation to abstract audios:

* AudioObject can easily created by providing an array of audio blobs
* Provides helpful methods such as cut, copy, paste, truncate

### How to start? ###

* The easiest way to start is to take a look at the example

### Limitations ###

* Only audio/wav is supported
* The script was successfully tested with Safari and Chrome

### License ###

Creative Commons Attribution 4.0 International