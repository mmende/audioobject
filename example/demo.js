/* global console, AudioObject, MediaStreamRecorder */

window.addEventListener('DOMContentLoaded',function(){

	// Load a AudioObject from a url
	var audioFromURL = new AudioObject('http://martins-mb-pro.local/audio-toolkit/song.wav', function(success){
		if(success)
		{
			//document.getElementById('audio-tag-2').src = audioFromURL.truncate(30).url();

			var x = audioFromURL.cut(64, 70);
			audioFromURL.paste(x, 3);

			//document.getElementById('audio-tag-2').src = x.url();
			document.getElementById('audio-tag-2').src = audioFromURL.url();
			
		} else {
			console.log('Could not load url...');
		}
	});
	/*
	var audioFromURL;
	new AudioObject('http://martins-mb-pro.local/audio-toolkit/song.wav', {sampleRate: 44100, channels: 1}, function(success, audioObject){
		if(success)
		{
			document.getElementById('audio-tag-2').src = audioObject.url();
		} else {
			console.log('Could not load url...');
		}
	});
	*/

	/** @type {Array} Holds the audio blobs */
	var audioData = [];
	var audio;

	// Now we initialize the mediaRecorder	
	var mediaRecorder = null;
	
	var mediaConstraints = {
		audio: true
	};

	function onMediaSuccess(stream) {
		mediaRecorder = new MediaStreamRecorder(stream);
		mediaRecorder.mimeType = 'audio/wav';
		mediaRecorder.sampleRate = 44100;
		mediaRecorder.bitRate = 16;
		mediaRecorder.audioChannels = 2;
		mediaRecorder.bufferSize = 256;
		mediaRecorder.ondataavailable = function (blob) {
			// Add the next blob to the audioData array
			audioData[audioData.length] = blob;
		};
	}

	function onMediaError(e) {
		console.error('media error', e);
	}

	navigator.getUserMedia(mediaConstraints, onMediaSuccess, onMediaError);


	// Finally we bind the buttons click events
	document.getElementById('start').addEventListener('click', function(){
		if(mediaRecorder !== null) {
			mediaRecorder.start(3 * 1000);
		} else {
			console.log('MediaRecorder is not initialized!');
		}
	});

	document.getElementById('stop').addEventListener('click', function(){
		if(mediaRecorder !== null) {
			mediaRecorder.stop();
			// Now that the recording has ended we can create an AudioObject from our audioData
			audio = new AudioObject(audioData);
			//audio.createWithData(audioData, {type: 'audio/wav'});
			console.log('Audio duration is ' + audio.duration());
			// Cut 1 second from start end end
			var newDuration = audio.truncate(3, 3).duration();
			//
			console.log('Audio duration is now ' + newDuration);
			document.getElementById('audio-tag').src = audio.url();
		}
	});

});