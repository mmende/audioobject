module.exports = function(grunt) {

	// Project Config
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		jshint: {
			files: 'src/**/*.js',
			options: {
				globals: {}
			}
		},
		concat: {
			options: {
				sourceMap: true
			},
			dist: {
				src: ['src/**/*.js'],
				dest: '.tmp/audio.object.min.js'
			}
		},
		uglify: {
			options: {
				compress: true,
				mangle: true,
				sourceMap: true,
				sourceMapIncludeSources: true,
				sourceMapIn: '.tmp/audio.object.min.js.map',
				banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' + '<%= grunt.template.today("yyyy-mm-dd") %>\n' +
						'* <%= pkg.homepage %>\n' +
						'* Copyright (c) <%= grunt.template.today("yyyy") %> ' + '<%= pkg.author %>\n' +
						'* Licensed <%= pkg.license %>\n' +
						'*/'
			},
			dist: {
				src: '<%= concat.dist.dest %>',
				dest: 'dist/audio.object.min.js'
			}
		},
		watch: {
			options: {
				livereload: true
			},
			scripts: {
				files: ['src/**/*.js'],
				tasks: ['scripts']
			}
		}
	});

	// Load plugin tasks for scripts
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	// Load the watch plugin task
	grunt.loadNpmTasks('grunt-contrib-watch');
	
	// Register scripts task
	grunt.registerTask('scripts', ['jshint', 'concat', 'uglify']);
	// Register the default task
	grunt.registerTask('default', ['scripts', 'watch']);
};